# **DOCUMENTATION**

--------------------------------
## **CA3 - Part1 - Virtualization**

A virtualização é uma técnica que permite  computadores simularem outros computadores como todos os seus dispositivos 
físicos. Isso é feito através do Hypervisor. Este por sua vez, é um software que cria uma abstração do hardware fazendo
uma gestão do mesmo. 

De maneira geral, a virtualização simula ambientes mais simples do que o real. É possível fazer a emulação de 
capacidades maiores do que o físico, entretanto, como a máquina real não tem a capacidade esperada, o processamento da
máquina virtual vai ser lento. Deste mesmo modo, é possível ter em uma mesma máquina várias máquinas virtuais 
diferentes com características diferentes.

Há dois tipos de virtualização. Há maneiras de o Hypervisor trabalhar diretamente sobre o hardware, chama-se 
___Hardware-level virtualization___. O outro tipo chama-se ___Hosted virtualization___, neste tipo de virtualização
o Hypervisor atua sobre um sistema operativo que atua como ___Host___ e as máquinas virtuais criadas atuam como
___Guest___. 

O hypervisor que estamos a usar nesse tutorial é o VirtualBox, este atua conforme o último tipo de virtualização e
precisa de um sistema operativo para correr. Ao iniciar a criação da máquina virtual em VirtualBox, é preciso nomear a
 mesma, escolher o ficheiro para salvar, o tipo de sistema operativo e a versão.

A seguir, deve-se selecionar as características do hardware virtual. Inicia-se definindo a quantidade de memória RAM, 
neste caso seria 2048MB. Logo após, deve-se selecionar a opção a cerca do disco rígido. neste caso há 3 opçóes, 
não criar um disco virtual, criar ou usar um já existente. Para este caso, é selecionada a opção de criar um virtual
com o tamanho base de 10GB. Uma vez selecionada essa opção é preciso selecionar o tipo de disco. Segue-se com a 
configuração básica. 

Em seguida, é preciso selecionar a dinâmica de alocação, pode ser alocado com o valor fixo, isso é
ocupar logo os 10GB do disco, ou a opção de alocação dinâmica, ou seja, conforme houver necessidade se aloca mais espaço
até o máximo do tamanho selecionado(10GB). Para este caso, seleciona-se a alocação dinâmica. Na sequência, é preciso
selecionar o ficheiro que será armazenado o disco virtual, nesse caso deixa-se o padrão sugerido. E está criada a 
máquina virtual!

Uma vez que é iniciada a máquina virtual, a máquina informa que é preciso ter um sistema operativo para operar a máquina
e sugere que instale algum software para proseeguir. Desta forma, faz-se o download de uma imagem mini do Ubuntu em:

    https://help.ubuntu.com/community/Instalation/MinimalCD

Através de um ficheiro ___mini.iso___ consegue-se iniciar a instalação do Ubuntu. Faz-se a instalação sugerida com suas
configurações iniciais, e acede-se um sistema operativo sem descktop gráfico. No terminal, inicia-se a configuração a 
fim de aceder a máquina virtual através da máquina host através dos comandos a seguir.


__APT__ é uma linha de comando útil para instalar, atualizar, remover e gerenciar pacotes deb em Ubuntu 
ou outros destribuidores Linux. 

    apt-get
    apt-cache

Estas complementações do commando ___apt___ são indicadas para acessar a diferentes versões e uma maior
gama de recursos. O comando ___sudo___ deve ser utilizado junto ao comando ___apt___ para atribuir-lhe os
privilégios de outro usuário, esse caso é uma exigência do Sistema Operativo Ubuntu uma vez que é desabilitado 
o super usuário a fim de exigir a utilização do comando ___sudo___ em todas as tarefas administrativas.

    sudo apt update

Com este comando é possível fazer a atualização das últimas versões dp repositório APT. A partir deste é
feito o download da lista de pacotes com as as atualizações, buscar as informações das próximas versões
e suas dependências.

![img.png](img.png)

No caso de não ser possível executar esse comando pelo facto de alguma pasta estar corrompida,
 é preciso remover a pasta local, ou todas as pastas corrompidas e executar o comando novamente para executar
 a atualização.

![img_1.png](img_1.png)

Após a atualização, é feita a instalação das ferramentas de rede para poder configurar a mesma conforme 
preciso para dar continuidade à tarefa. Para tal, é utilizado o seguinte comando:

    sudo apt install net-tools

Já a configuração é feita através de um arquivo ___.yaml___ em ___/etc/netplan/___ o qual armazenará as 
configurações pretendidas. Para isso usamos o comando a seguir:

    sudo nano nomeFicheiro

E em seguida acrescentamos as informações da configuração pretendida.

![img_2.png](img_2.png)

Uma vez definidas as configurações, é preciso aplica-las. Nesse sentido usamos o seguinte comando:

    sudo netplan apply

Essa configuração irá persistir entre ___reboots___.

Uma vez definidas as configurações, é preciso fazer a instalação do software ___openssh-server_ que por sua vez 
permite o acesso remoto a máquina virtual a ser configurada. E para isso é utilizado o seguinte
 comando:

    sudo apt install openssh-server

Após a instalação é preciso editar a configuração no arquivo ___/etc/ssh/sshd_config___. Neste caso, 
será ativada autenticação através de password. Usa-se 
o seguinte comando para acessar o arquivo:

    sudo nano /etc/ssh/sshd_config

Se for necessário, é solicitada a password do ___admin___ para acessar o arquivo para edição.

Para efetuar a alteração é descomentada a seguinte linha do arquivo:

- PasswordAuthentication yes

![img_3.png](img_3.png)

Após alteração feita, utiliza-se o seguinte comando para por em funcionamento as alterações realizadas 
no arquivo.

    sudo service ssh restart

Logo a seguir, faz-se a instalação do software ___vsftpd___, este permite criar ficheiros remotamente.
Para isso usa-se o seguinte comando:

    sudo apt install vsftpd

Em sequência é preciso editar o ficheiro de configuração a fim de permitir a escrita. Para isso entramos no 
ficheiro de configuração ___vsftpd.conf___ através do seguinte comando:

    sudo nano /etc/vsftpd.conf

Para efetuar a alteração é descomentada a seguinte linha do arquivo:
- write enable=YES

![img_4.png](img_4.png)

Após alteração feita, utiliza-se o seguinte comando para por em funcionamento as alterações realizadas
no arquivo.

    sudo service vsftpd restart

Feito isto, já é suposto ser possível acessar a máquina virtual para trabalhar. Esse acesso é feito
 através do uso do nome do usuário + @ + o IP da máquina virtual, cnforme o exemplo a seguir:

    nomeUsuario@192.168.56.5

Será solicitadad a password do usuário e, uma vez inserida a password correta, irá aceder à VM criada 
e irá aparecer o nome do usuário de modo diferente do padrão a indicar que está na VM.

![img_5.png](img_5.png)

Há um possível erro nesse momento que pode ter passado desapercebido, caso tenha sido criadas outras máquinas
virtuais que utilizaram o mesmo IP, o ssh não considera isso seguro e portanto não permite o acesso à máquina virtual.
Para contornar essa situação, deve aceder ao seguinte arquivo na máquina host:

    ./.ssh/known_hosts

Uma vez dentro do ficheiro, deve-se apagar a linha correspondente ao IP definido na máquina virtual. Este procedimento 
não é o mais seguro, de facto é uma melhor maneira utilizar outro IP, mas neste caso é uma solução para contornar o 
problema em questão.

Após acessar a máquina virtual pelo Host, será acessado o projeto do repositório remoto. Como não tem-se o git e o java
instalados na máquina virtual, deve-se instalar com os seguintes comandos:

    sudo apt install git
    sudo apr install openjdk-11-jdk-headless

Com estes softwares instalados, fazemos o clone do repositório remoto para a máquina virtual através 
do seguinte comando:

    git clone repositoryAddress

Após o comando __clone__, se acede a pasta do projeto maven para tentar correr o programa. Acede-se a pasta __basic__
e utiliza-se o seguinte comando para iniciar o programa:

    ./mvnw spring-boot:run

Neste momento encontra-se dois erros possíveis. Em um primeiro momento, a máquina não permite executar o comando por 
faltar a permissão para isso. De acordo com o padrão do Ubuntu, quando as permissões não são definidas, as permissões 
de leitura, gravação e execução são negadas. Para identificar as permissões, utiliza-se o comando __list__ a seguir:

    ls -l 

Nota-se que as notações de permissão são simbolicas e ficam a esquerda da lista, são representados por caracteres que 
seguem o seguinte padrão:

![img_6.png](img_6.png)

A fim de reverter o problema de execução deste projecto, utiliza-se o seguinte comando:

    chmod u+x nomeDoDiretório

A seguir, o segundo erro que pode ocorre é em relação ao maven. Pode ocorre, por algum motivo, que o programa não
corra porque o Maven não está instalado na máquina. O mesmo pode acontecer para o Gradle, uma das formas de reverter 
isto é instalar o Maven ou o Gradle na máquina através dos seguintes comandos:

    sudo apt install maven
    sudo apt install gradle

Uma vez resolvidos estes problemas, inicia-se os testes dos projectos __gradle_basic_demo__ e __spring boot tutorial
basic project__. Nestes casos, surge, basicamente, dois problemas relacionados a interface gráfica.

Quando corre-se o projecto __spring boot tutorial basic project__, tanto no projecto Maven quanto no projecto Gradle,
não consegue-se verificar o funcionamento concreto da aplicação uma vez que não há ferramentas para aceder a web.

Para correr o software e ter acesso ao seu funcionamento é preciso acessar a máquina virtual via terminal do Host, 
desta forma é possível correr a aplicação e acessar a interface gráfica do Host e verificar o funcionamento correto 
da aplicação.

![img_7.png](img_7.png)

O segundo problema é em relação ao projecto __gradle_basic_demo__, este tem a relação entre o Servidor e o Cliente 
afetada uma vez que sem interface gráfica na máquina virtual não há como ser aberta a janela do chat de comunicação
do Cliente. Nesse caso, é preciso alterar o localhost da __Task runClient__ para o IP da máquina virtual. Assim, 
põem-se a máquina virtual a correr o servidor e na host é acessado o chat do Client. 

--------------------------------
### **CA3 - Part1 - ALTERNATIVE**