# **DOCUMENTATION**

--------------------------------
## **CA3 - Part2 - Vagrant**

A dar seguimento a tarefa anterior, é apresentado o software Vagrant como uma ferramenta que proporciona o uso de 
máquinas virtuais mais facilitado. Vagrant apresenta uma solução mais simples, constituído por uma série de 
funcionalidades para o gerenciamente de máquinas virtuais. Pode-se destacar 3 funcionalidades mais relevantes: 
Provider, Provisioner e Box.

   - __Provider__ :
O provedor tem a função é de auxiliar na construção do ambiente virtual.
   - __Provisioner__ :
Por outro lado, o provisionador é responsável pela automatização da configuração do ambiente virtual. 
   - __Box__ :
consiste em um arquivos que contêm um sistema operacional com pacotes básicos pré-instalados.

O Vagrant possuí uma secção de disponibilização de boxs já pré-configuradas de rápida implementação.

    https://app.vagrantup.com/boxes/search

Neste link é possível encontrar máquinas virtuais como diversas configurações destintas que acelera o uso para o 
desenvolvedor. 

O Vagrant contém uma integração com o __Virtual Box__ que possibilita o provisionamento e provimento 
de computação em nuvem a permitir criar um ambiente completo e configurável, nesse sentido, é preciso ter o __Virtual
Box__ instalado na máquina. Para isso usa-se o seguinte comando:

    sudo apt-get install virtualbox

Uma vez que tem-se instalado na máquina o __Virtual Box__, deve-se fazer a intalação do próprio __Vagrant__. 
Para isso usamos o comando a seguir:

    sudo apt-get install vagrant

Uma vez com o software instalado na máquina, deve-se criar ou aceder a pasta em que serão armazenados os ficheiros
de utilização vagrant. No caso de criação de uma nova pasta, usa-se os seguintes comandos:

    mkdir nameDir
    cd nameDir
    vagrant init 

Após a criação da pasta, o commando __init__ faz a criação do ficheiro __vagrantfile__. Neste momento, há a opção de 
definir logo a máquina virtual a adicionar o nome do da box que é suposto usar. Por exemplo:

    vagrant init envimation/ubuntu-xenial

Caso não seja utilizado o comando __init__ com o nome é preciso adicionar a box para o vagrant. Para que isso ocorra 
é utilizado o seguinte comando:

    vagrant box add envimation/ubuntu-xenial 

Uma vez que temos o __vagrantfile__ com  a imagem da box que será utilizada, basta fazer o seguinte comando para iniciar 
a máquina virtual:

    vagrant up

Para fazer entrar na máquina virtual sem autenticação, utiliza-se o seguinte comando:

    vagrant ssh

Para reiniciar a máquina virtual, utiliza-se o seguinte comando:

    vagrant reload

Para finalizar o trabalho na máquina virtual e desliga-lá, usa-se o seguinte comando:

    vagrant halt

Caso não seja esse o objetivo, e queira suspender o funcionamento e salvar o estado que está
para reincia-lá de onde parou, utiliza-se o seguinte comando:

    vagrant suspend

Nete caso, para reiniciar a máquina virtual com a imagem que foi salva no último acesso
, utiliza-se o seguinte comando:

    vagrant resume

O seguinte comando remove a máquina virtual do Vagrant. É utilizado somente quando o 
objetivo é eliminar um projeto definitivamente.

    vagrant destroy

o comando a seguir faz a execução restrita do provisioner, ou seja, o ambiente não é 
reiniciado. Em outras palavras, ele atualiza as configurações do sistema de maneira 
agilizada.

    vagrant provision

Ao analizar o Vagrantfile, percebe-se que existem dois pontos iniciais para configurar o seu funcionamento. São eles;

    .configure
    .box

Enquanto a box é a imagem da máquina virtual pretendida, o __.configure__ guarda essa informação no arquivo 
__vagrantfile__ a fim de poder criar a máquina virtual a partir da cópia da imagem da máquina virtual disponibilizada. 
Mais que isso, o __.configure__ é responsável pro guardar todas as informações de configuração.

O aprovisionamento citado a cima, são definidos aqui através de instruções de comandos a serem executados quando da
criação da máquina virtual que é escolhida. Para efeturar esse script de comandos, utiliza-se:

    config.vm.provision

![img_1.png](img_1.png)

Na altura de configurar a(s) máquina(s) virtual(is) utiliza-se:

    config.vm.define

Essa instrução de configuração tem por objetivo definir as configurações de uso da máquina virtual, tais como nome, 
sistema operativo, hostname, rede e ip, portas de acesso, e tudo que for necessário para o objetivo do uso da máquina 
virtual em questão. No âmbito do exercício prático deste manual, foram configuradas duas máquinas para atuarem, uma como
banco de dados e outra responsável por correr a aplicação.

![img_2.png](img_2.png)

Nas definições da máquina responsável pelo banco de dados, além das questões de rede citadas a cima, é preciso definir
um aprovisionamento que permita a interação com API REST que suporte protocolos HTTP(S) e FTP(S).

![img_3.png](img_3.png)

Para se acesser à base de dados é necessário usar a URL: jdbc:h2:tcp://192.168.33.11:9092/./jpadb e por fim 
configura-se o comando __provision__ para ser executado sempre a correr e definir o caminho para as classe 
através do seguinte comando:

![img_4.png](img_4.png)

A máquina responsável por rodar a aplicação tem IP diferente e é definido o provider como hypervisor virtualbox e 
adiciona-se mais memória RAM a fim de rodar a aplicação. Define-se as portas de acesso para o tomcat e defini-se o
script de aprovisionamento da máquina.

![img_5.png](img_5.png)

Além das configurações da máquina, é intruido que a máquina faça o clone do repositório da aplicação, desse modo,
ao invés de descarregar ou enviar a aplicação por inteira, basta enviar o ficheiro __vagrantfile__ que é possível
acessar as funcionalidades da aplicação.

Deste modo, após fazer o clone, é instruido o direcionamento para a pasta correta da aplicação, dadá as permissões
para executar os comandos necessários, passa-se o comando a seguir para fazer __build__ da aplicação;

    ./gradlew clean build

Por fim, é preciso atualizar o directório do ficheiro __.war__ uma vez que é criado sempre no primeiro momento build
realizado da aplicação.

Mais comandos de Vagrant e as suas utilidades no terminal podem ser acessados através do comando __vagrant --help__.

![img.png](img.png)

Para acessar as máquinas virtuais utiliza-se o browser do host com os seguintes endereços:

![img_6.png](img_6.png)

--------------------------------
### **CA3 - Part2 - ALTERNATIVE**

VMWare

VMWare situa-se como concorrente do Virtul Box em soluções de virtualização. É um software hypervisor, tal como o 
VirtualBox, também suporta vários sistemas operativos, mas há alguma diferença do concorrente. E também partilham 
de recursos de rede e memória.

Com VMWare é poss+ivel criar uma rede exclusiva, multi-cloud e digital worksplace. Tem uma maior eficiência com 
optimização na utilização de dados, facilidade de reverter aprocisionamentos e facilidade na criação de máquinas 
virtuais.

Contudo, há algumas discrições comparativas diferentes entre os dois. Enquanto o VirtualBox é um software open source, 
o VMWare é apenas para uso pessoal, havendo uma versão gratuíta com funcionalidades limitadas. 
Enquanto o VirtualBox está disponivel para vários sistemas operativos, o VMWare está diposnível gratuitamente
apenas para Linux e Windows, para outros sistemas operativos somente é disponibilizada a versão paga. E quanto aos 
sistemas operativos das máquinas virtuais que suportam, o VirtualBox tem maior variedade do que o VMWare que só suporta
Linux, Windows, Solaris e Mac.
Enquanto o VirtualBox suporta formatos de disco virtual VDI,VMDK e VHD, o VMWare só suporta VMDK.
Enquanto o VirtualBox suporta snapshots para restaurar o estado da máquina virtual, o VMWare só suporta em verões pagas.
Enquanto o VirtualBox é open source, o VMware não é.

De fato o Virtual box pode parecer ter mais vnatagens que o VMWare, entretanto o trade off disso é o desempenho, com 
menos funcionalidades o VMWare acaba por ter máquinas um pouco mais rápidas e eficientes que as do VirtualBox o que 
pode trazer vantagens a depender da sua utilização.

Foi implementada um __Vagrantfile__ para a versão com VMWare onde foram feitas alterações no campo de configuração 
dos IPs e do provider. A pasta onde foi alocado esse recurso é __./CA3/Alternative__.


