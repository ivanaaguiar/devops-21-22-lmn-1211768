# **DOCUMENTATION**

--------------------------------
## **CA1 - Part1 - GIT** 

In this first week is supposed to use the ___Git___ to resolve the requirements for this exercise.
In the first moment will be developed without branches, that's mean to use the master/main branch 
(default branch) only.

Git is a version control system (VCS), VCS is responsible to record changes to a set of files over time
given possibility to access a specific version any moment in the future. That allows to revert files, 
compare changes, repair some problems or revert the entire project easily if was necessary. 

### - Configuration -


First, should be installed and configured the Git. Should be configured the username and email address
to be identified the responsible for the commits, and it's immutable baked into the commits that starts
to create.

Using the command ___git config --list___ is possible to see the all configuration settings.

|$ git config --global user.name "Example Example"|
|-------------------------------------------------|

|$ git config --global user.email xxxx@email.com  |
|-------------------------------------------------|

Get and set repository or global options

In this point is important to pay attention which that command works without ___--global___. 
In this case, must be in the project to use the command, and once is used the ___--global___ that
configuration will always be used by the Git for anything did on that system.

To configure the name or email is used the command ___user.name___ or ___user.email___ plus the 
respective data. 

After to configure the git, to create a copy of remote repository at the local repository the responsible
command is that:

|git clone urlRepository|
|------------|

That command will make a full copy of all data that the server has. Is possible to specify the directory 
name as an additional argument:

|git clone urlRepository directoryName|
|------------|

When is started a change at the project, that pass to state of untracked. That means the git do not know 
about that changes. To make git knows about the changes is needed to pass the file to the staged to be 
committed, for this is used the command:

|git add .|
|------------|

To see the status of the changes is used the command:

|git status|
|---|

Once used that command will be able to see if the file is tracked or untracked to be committed. If is
tracked is able to use the command:

|git commit branchName|
|---|

To be committed the changes is expected a commit message inline with the command, that can be made with
___-m___:

|git commit -m "message" branchName|
|---|

That command will make the git take a snapshot of the files and identify how many files were changed, 
and statistics about lines added and removed in the commit. That will generate an output as SHA-1 
checksum, that looks like something like (463dc4f) and that works as an identifier of the commit.
Is possible to skip the staging area using the ___-a___:

|git commit -a -m "message" branchName|
|---|

With this command is possible to skip to use the command ___git add .___, in this option is used 
both steps in one.

To view the commit history is used the command:

|git log|
|---|

By default, is listed the commits made in that repository in reverse chronological order and the most recent 
commits show up first. Each commit is showing with its SHA-1 checksum, the author’s name and email, the date
written, and the commit message.

When there are a point that would be shared with the remote repository, after the commits are made, is used 
the command:

|git push branchName|
|---|

If nobody has pushed in the meantime, this command does not work. To check if it has a new work has been pushed
to the remote repository since it has cloned, is used the command:

|git fetch branchName|
|---|

If the current branch has something new, then is possible to use the command:

|git pull branchName|
|---|

That command will marge the local repository with de remote repository and show, if necessary, the conflicts 
to be solved.

Is important to tag specific points in a repository’s history to identify the system version.
Typically, is used version as major.minor.revision (e.g. v1.0.0, v2.0.0 and so on). To create at the local 
repository a new tag is used the command:

|git tag tagName hashCommit|
|---|

In the another way, to delete the tag at the local repository is used the command:

|git tag -d tagName hashCommit|
|---|

As the commits, is possible to add a message to the tag using ___-m___. Likewise, to send the tag to the
remote repository is using the same command with the difference of to use the ___origin___ and ___tagName___.

|git push origin tagName|
|---|

In the another way, to delete the tag at the remote repository is used the command:

|git push --delete origin tagName|
|---|

If the goals needed a new repository to work, that is possible using the command add:

|git remote add shortname url|
|---|

--------------------------------
### **CA1 - ALTERNATIVE**

------------------------------------------------------------
## **CA1 - Part2 - GIT**

-----------------------------------------------------------
The default branch in Git is master. Every commit which is made, the master branch points to the last commit was 
made. Every time you commit, the master branch pointer moves forward automatically. Is important to clarify that
the master branch is not a special branch, It is the first branch created by default, but it is exactly like any
other branch. 

To create a new branch at the local repository is used the command:

|git branch branchName|
|---|

After the branch is no longer needed, uses that command to delete the branch, otherwise the branch will stay to
exist in the repository. To delete the branch at the local repository is used the command:

|git branch -d branchName|
|---|

As the tag's command, to share and to delete is used the same commands, the only difference is replaced the tag name
for the branch name. To share the branch at the remote repository is used the command:

|git push origin branchName|
|---|

And to delete the branch at the remote repository is used the command:

|git branch --delete origin branchName|
|---|

When the repository has more than one branch, the HEAD shows which branch is the current branch. By default, the head
starts in the master branch. To change the HEAD to the chosen branch is used the command:

|git checkout branchName|
|---|

**Obs**: if is made git log, that will show the commits of the actual branch, if the goal was to see all the commits,
is supposed to use the command: 

|git log --all|
|---|

To merge the changes is used the git command ___merge___. That is made at the chosen branch as HEAD and will make the marge of the branch indicated,
if it has conflicts, that will be indicated at the code with **"<<<<<<<<"**, **"========"** or **">>>>>>>>"**
the conflicts with all commits. After to solve the conflicts, just will be share with the remote repository using
the commands  **git commit** and **git push**.

|git merge branchName|
|---|

To change the name of the branch locally is used the command:

|git branch --move badBranchName correctedBranchName|
|---|

To share the correct branch name is used the command:

|git push --set-upstream origin correctedBranchName|
|---|

Once the correct branch name is shared, the bad branch name still in the remote repository. That means which is 
necessary to delete that using the follow command:

|git push origin --delete badBranchName|
|---|

It is important to pay attention that is not indicate to change the branch name when have had collaborators working
at the same time.

--------------------------------
### **CA1 - Part2 - ALTERNATIVE**

