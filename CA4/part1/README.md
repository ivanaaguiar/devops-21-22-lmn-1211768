# **DOCUMENTATION**

--------------------------------
## **CA4 - Part1 - Virtualization**

### Containers e Dockers

Containers consistem em __Container Engines__ que permitem gerir os containers. Estes por sua vez, permitem 
empacotar uma aplicação e todas as dependências desta aplicação. Neste caso são diferente de máquinas virtuais 
que simulam os hardwares e os sistemas operativos. 

Em outras palavras, os containers permitem correr aplicações com versões das dependências diferentes das instaladas
na máquina e essas dependências não conflitam entra si. Os containers executam de maneira isolada, 
podem correr em vários sistemas operativos e são transportáveis. 

A configuração dos containers é feita através do __Dockerfile__. Este é um ficheiro de texto com intruções para 
executar e correr o que for necessário para a função esperada. Nesse sentido, os containers são utilizados
muitas vezes para executar micro serviços e tornar independentes os processos de uma aplicação.

O __Dockerfile__ é considerado uma imagem do container que queremos utilizar. 

Os comandos mais comuns são encontrados no terminal através do seguinte comando:

    docker --help

A partir do seguinte resultado vamos utilizar alguns comandos para desenvolver este exercício.

![img_1.png](img_1.png)

Para iniciar a utilização dos containers, foi preciso fazer a isntalção da aplicação __Docker Desktop__ 
e, em seguida, fazer o registro para utilizar o repositório docker a fim de guardar nossas imagens. Isso é 
feito através do link a seguir:

    https://www.docker.com/

Uma vez intalado o __Docker Desktop__, cria-se uma imagem docker, através do arquivo __Dockerfile__ para correr
a aplicação __ChatApp__. Inicia-se o exercício pelo __partA\Dockerfile__.

![img.png](img.png)

Ao iniciar as instruções no arquivo __Dockerfile__, tem-se em mente a estrutura de escrita para facilitar a 
compreensão das instruções. Por convenção, as instruções são escritas em letras maiúsculas, mas isso não interfere
diretamente no funcionamento das instruções.

    FROM ubuntu:18.04

A instrução __FROM__ tem por objetivo iniciar com o download de um imagem através do repositório __dockerhub__ e
dar instruções subsequentes a ela para configura-lá como for necessário. 

Em seguida utiliza-se os seguintes comandos:

    RUN apt-get update
    RUN apt-get install -y apache2
    RUN apt-get install openjdk-11-jdk-headless -y
    RUN apt-get install -y git

Aqui vê-se a instrução __RUN__, esta tem por objetivo executar a qualquer comando e faz o commit do resultado. Ou seja,
não segue as intruções por linha, é uma boa prática que cada comando __RUN__ seja em uma linha única.
Vale salientar que, por padrão, o comando __RUN__ executa o comando por padrão em um shell.

A seguir dá-se intstruções para extrair a aplicação que deseja-se executar do seu repositório.

    RUN git clone https://bitbucket.org/luisnogueira/gradle_basic_demo.git
    WORKDIR gradle_basic_demo/
    RUN chmod u+x gradlew
    RUN ./gradlew clean build

Primeiramente, faz-se o clene do repositório. A seguir usa-se a isntrução __WORKDIR__, esta tem por objetivo configurar
o diretório de trabalo das instruções a seguir. Como não se está a trabalhar na sequência de linhas como é a linha 
de comando, deve-se dar a instrução para definir  o caminho absoluto para que sejam aplicadas as instruções ao local 
correto. Nesse sentido, como quer-se executar a aplicação dentro de um diretório que fez-se clone, deve-se dar o 
caminho para direcionar os comandos que iniciem a aplicação.

Por outro lado, o comando __chmod u+x gradlew__ é definido com a instrução __RUN__ a fim de dar permissão para executar
a aplicação em gradle que fez-se clone. E em seguida se pede para fazer __build__ e iniciar a aplicação automaticamente
quando o __container__ iniciar. Uma vez que a aplicação irá disponibilizar um servidor para conectar a clientes do chat, 
deve-se definir a porta do servidor em utilização.

    EXPOSE 59001

A instrução __EXPOSE__ é responsável por isso. Essa instrução indica ao Docker quais as portas que o container estará
a ouvir as conexões. Esta não é o mesmo que a flag __-p__ do comando __docker run__ que será exposto a seguir, esta 
somente é direcionada ao __Dockerfile__ uma vez que o container é criado sem a publicação de portas, e para isso usa-se
a flag __-p__ ou __--publish__ para definir essa porta.

    CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

Por fim, usa-se a instrução __CMD__ para executar o comando que queremos e iniciar a aplicação. Neste caso é executado 
o comando para iniciar o servidor do chat com a porta que definiu-se anteriormente. Aqui, vale ser ressaltado que se 
forem utilizadas várias instruções __CMD__ no mesmo __Dockerfile__ apenas a última terá efeito. Caso seja necessário,
há a alternativa de combinar esta com a intrução __ENTRYPOINT__ que funciona com o mesmo objetivo. Neste caso, a 
intrução __CMD__ defini os argumento que serão executados e a insturção __ENTRYPOIT__ irá fazer uso desses argumentos.
Dessa forma, deve-se sempre optar por utilizar a primeira opção, entretanto, quando for preciso que o container execute
algum comando sempre que iniciar, existe esta maneira que facilita a leitura para os desenvolvedores.

Uma vez que tem-se o __Dockerfile__, direciona-se o terminal até o diretório onde está guardado e usa-se o seguinte 
comando:

    docker build -t 1211768/gradle_demo_build .

![img_2.png](img_2.png)

Com o comando build irá fazer a execução do __Dockerfile__ e as instruções que contém. Já a flag __-t__ tem por
responsabilidade criar a flag da imagem que neste caso é denominada como __1211768/gradle_demo_build__. O ponto ao 
final do comando é relacionado ao path e significa que o contexto para geração da imagem é da pasta atual.

    docker images

Uma vez que tem-se a imagem criada, pode-se verificar quais imagens existem no __Docker__ atrás deste comando. Isso 
facilita o trabalho quando ten-se difereças imagens diferentes e é preciso identificar ou buscar informações básicas 
sobre determinada imagem. Assim, quando já é identificada a imagem que irá executar, utiliza-se o seguinte comando:

    docker run -p 59001:59001 -d --name part1 1211768/gradle_demo_build

Quando faz-se o comando __run__ para executar a imagem o resultado é o __container__. Aqui faz-se uma distinção 
que muitas vezes acaba por ser um mal entendido. A imagem é um pacote executável que inclui tudo o que é necessário 
para executar uma aplicação, enquanto o container consiste em um processo em área restrita que executa um aplicativo
e suas dependências no sistema operacional host.

Aqui é preciso ressaltar algumas situações. Inicialmente, se utilizar apenas o comando __docker run imageName__ irá
funcionar normalmente. No caso do servidor que é expectado neste exercício, utiliza-se a flag __-p 59001:59001__ a
fim de definir a porta da máquina host  e da aplicação __container__. Ou seja, este parâmetro é responsável por criar
uma série de regras de encaminhamento para acessar o __container__ através da porta 59001.

Além disso, no caso de executar-se o comando sem a flag __-d__ a aplicação iria inviabilizar a utilização da aba 
atual terminal e nao permitiria nem dar algum comando para parar o funcionamento do __container__. Ou seja, __--detached__
permite o funcionamento "desanexado" no terminal.

E por último, quando incluí-se __--name__ seguido de um nome, está a dar-se um nome para o __container__ que por sua vez irá
facilitar a manipulação de __containers__ em um âmbiente com elevado número destes. Quando precisa-se visualizar
quais os __containers__ estão a correr, utiliza-se o seguinte comando:

    docker ps -a

Nesse contexto, a flag __-a__ tem por objetivo apresentar todos os __containers__ existentes, caso contrário ira ser
apresentado apenas os __containers__ que estão a corre. 

    docker exec -it 1211768/gradle_demo_build /bin/bash

Este comando, por sua vez, é responsável por executar comandos dentro de um __container__ que já está a correr. Nesse 
sentido, caso as atividades do __container__ sejam pausados e reiniciados, o comando __exec__ não é reiniciado juntamente
com o __container__. É possível adicionar opções ao comando como neste caso está a ser incluido o __-it__.

![img_3.png](img_3.png)

Uma vez que o __container__ está a correr, deve se ir na máquina host e executar o seguinte comando gradlew
no diretório onde está a aplicação:

    gradlew runClient

Este por sua vez irá abrir o chat pelo cliente e se comunicar com o servidor em conformidade com a configuração.
    
Depois de tudo a funcionar, para enviar as __imagens__ ao repositório __dockerhub__ é preciso fazer os 
seguntes comandos:
    
    docker push user/name:<tag>

Nesta etapa, há considerações a serem feitas uma vez que esse comando pode gerar alguma confusão. Para fazer __push__ 
da __imagem__ +e necessário que seja identificado o usuário, o nome da imagem e definir uma tag, que por padrão é 
considerada __lastest__. Caso a __imagem__ seja construida apenas com o nome, será preciso executar o seguinte comando
a fim de identificar o usuário:

    git tag <imagemID> user/name:<tag>

Isso significa que a __imagem__ pode ter um nome local, e seja feita uma tag diferente para ela na altura de definir 
um versionamento para enviar ela ao repositório remoto. E após definir a tag, é possível fazer, enfim, o __push__. E
ao repetir o comando com usuário, nome e tag, podem ser definidos diferentemente do host. Isso acaba por criar alguma 
confusão para quem nunca trabalhou com dockers e repositório remoto como o __dockerhub__.

Uma boa prática para evitar esse problema é que ao construir a imagem seja seguido o padrão para o push com user, 
nome do repositório (se já existente) e a versão (tag) da imagem. Assim ao ser criada, já é possível fazer o __push__
 visto que satisfaz os requisitos do comando.

As __imagens__ deste exercício estão disponíveis no seguinte link:

    https://hub.docker.com/u/ivanaaguiar

![img_5.png](img_5.png)

A nível de exercício, há uma segunda maneira possível de implementar o __dockerfile__ que fez através do 
__partB\Dockerfile__. Este tem uma pequena modificação em relação a __partA__.

![img_4.png](img_4.png)

Enquanto na __partA__ damos intruções para preparar o âmbite de modo que as dependências da aplicação sejam satisfeitas,
na __partB__ apenas é feito o update e intalado o __JDK11__ e dada a instrução __COPY__ do ficheiro executável __.jar__ 
para ser copiado dentro da __imagem__ e em seguida difinido o __EXPOSE__ e o __CDM__. Com isso, ten-se o mesmo resultado
que o __partA\Dockerfile__.
