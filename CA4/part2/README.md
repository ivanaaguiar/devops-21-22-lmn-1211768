# **DOCUMENTATION**

--------------------------------
## **CA4 - Part2 - Virtualization**

### Compose Containers

Para esse exercício, será demandada a criação de dois __Dockers__ que desempenham fuções destintas e necessárias para
que uma aplicação consigo correr corretamente. Um __Docker__ irá correr a aplicação como servidor, e está será chamada
de __web__, enquanto o outro __Docker__ irá correr a base de dados e, portanto, será chamada de __bd__.

Para que isso seja possível, inicialmente são criados os __Dockersiles__ de ambas para que depois sejam construidas as 
__imagens__ respectivas.

![img.png](img.png)

Este __Dockerfile__ é responsável por criar a versão __web__ o qual irá ter como ponto de partida uma __image de Tomcat__
compatível com __JDK8__. Caso seja utilizada última versão do tomcat é possível que hajam incompatibilidades a depender
da versão do JDK uitlizado no __docker__ responsável pela base de dados. Esse é um dos benefícios que os __Dockers__ 
apresentam, idependente da configuração existente na máquina host, o ambiente configurado nos containers podem 
satisfazer as necessidades da aplicação de forma direta e sem gerar conflitos com a máquina.

Em sequência à imagem do tomcat, são feitas as atualizações e instalações necessãrias para o funcionamento da aplicação,
limpa-se o cache do sistema, cria-se o diretório onde será feito o clone da aplicação, direciona o container para o 
local da aplicação, dá-se as permissões necessárias e por fim põem-se a corre a aplicação com exposição 
na porta padrão 8080.

![img_1.png](img_1.png)

Este __Dockerfile__ é responsável por criar a versão __bd__ o qual irá ter como ponto de partida uma __imagem de 
ubuntu__. É feita as atualizações e instalões necessárias para rodar a base de dados, direciona-se o container para 
o local onde é armazenado o ficheiro com a base de dados, defini-se a exposição do container e põem-se a correr a 
base de dados.

Uma vez que temos os __Dockerfiles__ definidos, é preciso criar um __docker-compose, ficheiro em yaml, para inicializar
as imagens.

    docker-compose.yml 
    docker-compose.yaml

![img_6.png](img_6.png)

No __docker-compose.yml__  inicialmente definimos a versão de __docker-compose__ que será utilizada no ficheiro, neste caso 
utiliza-se a última versão (versão 3). 

Em seguida, define-se os serviços/recursos que serão utilizados. Aqui, entende-se serviços/recursos como 
imagens/containers pré-definidos que serão executados com o compose.

Neste exercício, serão utilizados o serviço __web__ e __bd__. Em ambos, o campo __build:__ tem por objetivo configurar
a criação das imagens. Já no campo __ports:__ é definida a configuração de exposição das imagens e as regras de 
direcionamento para acessar os containers.

Já no campo __networks:__ define-se estaticamente o endereço IP de cada imagem para ligar-se a rede que será definida 
em seguida. Existe outra maneira de fazer essa configuração caso não seja feita no ficheiro compose. Para isso são 
utilizados os seguintes comandos:

    docker network create<network-name>
    docker connect <network-name><container-name>
    docker run --network <>network-name><>

Especificamente no serviço bd, é definido __volumes:__ com a função de criar uma cópia da base de dados no local 
estipulado a fim de persistir as interações com a base de dados. Para enviar essa cópia ao repositório remoto, deve-se
incluir o contúdo ao commit.

Em seguida, é definida, através de __networks:__, a criação da rede e sua máscara de rede que será utilizada 
pelas imagens construidas.

Uma vez que tem-se estruturado o __docker-compose.yml__, para fazer a construção das imagens utilizamos o seguinte 
comando:

    docker-compose build

![img_3.png](img_3.png)

Caso haja algum erro, aparecerá na execução das camadas de build da imagem com erro, se forem erros referentes à imagem. 
Com tudo a funcionar, executa-se o comando a seguir para fazer correr as imagens.

    docker-compose up

Na sequência, alguns comandos que segue o mesmo mecânismo da part 1 deste exercício. 

    docker-compose star/stop <service>
    docker-compose ps
    docker-compose exec nagios sh 
    docker-compose logs <service>
    docker-compose rm <service>
    docker-compose images

Diferente do __Dockerfile__ pode-se definir os requisitos para fazer push das imagens no próprio ficheiro
compose. Através do campo __image:__ logo a seguir ao campo __build:__. Dessa forma, basta utilizar o seguinte 
comando para fazer push ao repositório remoto:

    docker-compose push

![img_5.png](img_5.png)

As __imagens__ deste exercício estão disponíveis no seguinte link:

    https://hub.docker.com/repository/docker/ivanaaguiar/docker-compose

![img_4.png](img_4.png)

--------------------------------
### **CA4 -  Part2 - ALTERNATIVE**

### Kubernetes

Essa ferramenta, tem vulgarmente chamada de K8S, é um sistema __open-source__ que tem como objetivo a orquestração de containers para automatização 
de deploys, escalonamento e gerenciamento de aplicações. Em alguma medida, pode ser confundido como um concorrente aos
__dockers__, entretanto o __Kubernetes__ atua intimamente com o __Docker__. Mesmo que este tenha um orquestrador próprio,
o __Docker Swarm__, ainda assim o __Kubernetes__ tem um papel relevante nessa função.

o __Kubernetes__ foi desenvolvido a pensar em escalabilidade, performace e otimização de recursos. Um dos benefícios 
dessa ferramenta é a capacidade de atuar sobre as inúmeras atualização de uma aplicação, sendo uma ferramenta compatível
com as metodologias ágeis atuais utilizadas no mercado automatizando as entregas de novas versões constantemente.

No que tange a orquestração de containers, essa ferramenta tem por objetivo trabalhar com um conjunto containers, 
o que vai ao encontro do exercício realizado. Na prática, isso significa na criação de __clusters__. Ou seja ter 
dezenas de containers a rodar em simultânio a mesma aplicação a balanciar e atender as requisições. Com a criação
e remoção de containers em um __cluster__ é feita de forma programada, o que seguinifica dizer que a partir do momento
que a aplicação necessita de mais recursos, automaticamente o orquestrador pode prover mais containers.

Nesse sentido, em alternativa, o __kubernetes__ trás a opção de __pods__ a constituir o __cluster__, onde separaria
a execução da aplicação e o armazenamento da base de dados similarmente ao que foi feito com o __docker-compose__.
Para o gerenciamento dos componentes é criado um plano de controle, onde são feitas as decisões globais que atuaram
sobre os eventos do __cluster__.

Os __pods__ são as menores unidades de computação implantáveis que podem ser criadas e gerenciadas no Kubernetes. Um 
__Pod__ é um grupo de um ou mais contêineres, com armazenamento compartilhado e recursos de rede e uma especificação 
de como executar os contêineres. O conteúdo de um pod é sempre co-localizado e co-agendado e executado em um contexto 
compartilhado e são fortemente acoplados. Além de contêineres de aplicativos, um Pod pode conter contêineres init que
são executados durante a inicialização do Pod. Você também pode injetar contêineres efêmeros para depuração se seu 
cluster oferecer isso.

A criação de __pods__ é similar ao que foi feito no __docker-compose__, a estruturação da informação é um tanto similar
e igualmente  em um ficheiro yaml. Os containtes que um __pod__ pode ter, são em abordagens __sidecar__, onde exitem 
containers auxiliares que servem ao container principal do __pod__.


![img_2.png](img_2.png)

No caso do exercício, é necessário criar um __pod__ para cada elemento da aplicação, portanto, minimamente seriam 
necessários dois __pods__ para desempenhar o mesmo papel do __docker-compose__ que foi feito no último exercício.
