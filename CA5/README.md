# **DOCUMENTATION**

--------------------------------
## **CA5 - Part1 - CI and CD Jenkins**


__Jenkins__ é uma ferramenta open source onde o objetivo é a __automação de stages em um ambiente de desenvolvimento__
de softwares. O trabalho em pipeline possibilita separar as etapas de building, testing e etc a fim de criar
uma constância em entregas e integração. Sua atuação suporta sistemas de controle de versões, como __Git__ que é
utilizado nesse exercício, uma grande gama de linguagens, como __Java__ que é utilizado nesse exercício, e contém uma 
variedade de plugins, dois quais podem gerar relatórios ou análises, oir exemplo, que agregam funcionalidades ao 
projeto em exercício gerando ainda mais possibilidades de testes.

A integração contínua (CI) consiste em integrar os desenvolvimentos diários de __features__ às atualizações de um 
software, consistindo, então, em entregas contínuas (CD). Os problemas que surgem a partir disto, são em manter um 
padrão de funcionamento e não gerar conflitos com as versões anteriors. Ou seja, manter a consistência de todas as 
etapas que envolvem o desenvolvimento, testes e produção. Para isso os pipelines são divididos em estágio que permitem
esse monitoramento.

A linguagem utilizada para execução do __Jenkins__ é Groovy e segue uma linha parecida com o __Vagrant__ e o __Docker__
ao utilizar de um __Jenkinsfile__ que armazena as instruções/stages que serão executados no processo de aplicação
da automatização. Pode ser estruturada de duas maneiras: __declarativa ou script__. Pode ser criado o script diretamente
no __Jenkins__ caso seja uma mais valia. Dentro da síntaxe declarativa, utilizada para esse exercício, encontramos
alguns termos importantes para o desenvolvimento das instruções:

- __job__: define uma pipeline seja ela completa ou apenas um etapa do processo de compilação;
- __pipeline__: é uma sintaxe específica que define um "bloco" contendo todo o conteúdo e instruções para executar
  todo o Pipeline;
- __agent__: é uma sintaxe específica que instrui o Jenkins a alocar um executor (em um nó) e um espaço de trabalho
  para todo o Pipeline;
- __node__: cria um workspace (uma pasta na máquina local) onde será compilado o projecto;
- __stage__: é um bloco de síntaxe que descreve um estágio deste Pipeline;
- __step__: é uma sintaxe específica que descreve as etapas a serem executadas em um determinado estágio.

Para este exercício, é preciso fazer o download do arquivo executável (.war) do __Jenkins__ no seguinte site:

    https://www.jenkins.io/download/
![img_7.png](img_7.png)

Este foi introduzida no próprio diretório desta documentação. No terminal do administrador, é introduzido o seguinte 
comando para executar o arquivo:

    java -jar jenkins.war

Assim, o jenkins inicia no porto 8080 por default e temos acesso em:

    localhost:8080

![img_8.png](img_8.png)

É gerado uma palavra passe do administrador inicial automaticamente no terminal que será introduzida no porto para 
iniciar as configurações.

![img_9.png](img_9.png)

Em seguida, define-se os dados do utilizador e instalados os plugins sugeridos. Ao acessar a __dashboard__ é iniciada
a criação de uma nova pipeline onde introduz-se o nome e seleciona a pipeline.

![img_10.png](img_10.png)

Na criação dá pipeline defini-se o __script from SCM__ para utilizar o __Jenkinsfile__, seleciona a opção para utilizar
o __Git__ e introduz o URL para o repositório do bitbucket. Nesse sentido, é preciso ter o repositório público para que
o __Jenkins__ possa ter acesso ao mesmo. O __branch__ que é utilizado por default nesse exercício é o __master__ e 
por fim passa-se o __script path__ para o __jenkinsfile__.

A partir disso, todas as alterações devem ser salva e executados os comandos __git__ para enviar as alterações ao 
repositório a fim de que o jenkins tenha acesso ao conteúdo do __jenkinsfile__.

![img_11.png](img_11.png)

Inicia-se as instruções com:

    pipeline {} 

E todas as instruções são geridas desntro desse bloco. A seguir definice o __agent__ que nesse caso é qualquer.
E posteriormente inicia-se os __stages__. Onde dentro do bloco é criado cada fase que será monitorada. Em cada 
etapa é impressa a nomenclatura da etapa através do comando __echo__.

O primeiro __stage__ é o __checkout__ onde irá ao repositório git indicado.

![img_1.png](img_1.png)

A seguir é feito o __assemble__, para isso indica-se o path que será executado o comando __Gradle__, utiliza-se
o comando __dir__ com o caminho para a aplicação. Este comando necessita de um script, e neste terá a instrução 
__bat__ seguida do comando __./gradlew clean assemble__ Isto porque se está a utilizar da um sistema operacional
__Windows__.

![img_2.png](img_2.png)

O terceiro passo é para correr os __testes__, segue o mesmo reciocício do __assemble__, possuí a path seguida
do comando para correr os testes. Esses dois passos são feitos para separar o processo de __build__ em duas etaps,
a inicialização do app e a verificação dos testes, caso contrário, o comando __build__ executaria ambos de uma só vez.

![img_3.png](img_3.png)

O quarto passo é feito o arquivamento dos ficheiros gerados no processo de build. É executada a instrução 
__archiveArtefacts__  com a path para a pasta onde se encontram os ficheiros __build__.

![img_12.png](img_12.png)

Após definidos os estágios, faz-se o bloco __post__ para arquivar os ficheiros __.xml__ dos resultados dos test que 
estão no diretório __test-results__.

E por último observa-se que todas etapas foram concluidas com sucesso. 

![img_4.png](img_4.png)


--------------------------------
## **CA5 - Part2 - Pipeline**

Na segunda parte do exercício, é dada a continuidade do __Jenkinsfile__ e acrescenta-se novos __stages__.

![img_13.png](img_13.png)

Neste estágio, é forçada a geração do arquivo __.jar__ que será utilizado após no __Dockerfile__. Para isso,
é feito o encaminhamento para o diretório onde está a aplicação e dá-se o comando para criação do arquivo.

![img_14.png](img_14.png)

No estágio __Javadocs__ é igual ao estágio anterior, dá-se o encaminhamento para a criação do diretório e seus 
ficheiros, seguido do script com o comando que executará o mesmo.

![img_15.png](img_15.png)

Em seguida, é definido o ficheiro __dockerfile__ e dadas as instruções para a sua criaçao seguida da 
publicação no repositório docker hub. Aqui houve diversas tentativas com insucesso que não foram percebidas.

    docker.withRegistry('https://registry.hub.docker.com', 'jenkinsfile')

Este comando faz a ligação com o repositório __hub.docker.com__. Para isso é preciso criar uma __credential__.
Os passo são:

__Dashboard__ > __Gerenciar Jenkins__ > __Manage Credentials__ > __global__ > __Add Credentials__

A seguir prencher os campos compatíveis com o repositório docker hub.

- Username with password; 
- Scope (Global) 
- UserName && Password (conforme o repositório)
- ID (ID a usar nos scripts)

Uma vez feito isso, o docker já passa a funcionar corretamente.

![img_5.png](img_5.png)

Parcebe-se que as imagens geradas pelo comando são criadas corretamentes, entretanto, ao fazer o camando 
__push__ pelo __jenkins__, não há um funcionamento de sucesso. Foram feitas exaustivas tentativas que não 
ocorreram bem. 

![img_6.png](img_6.png)

Ao final, percebeu-se que se retirasse apenas o __push__, funcionava tudo corretamente. 

![img_17.png](img_17.png)

A instrução entra em um loop para tentar enviar e não executa o comando. Como há uma data limite de entrega
deixou-se passar esse erro para entregar em tempo hábil, entretanto ainda há tentativas para perceber o erro e
conserta-lo.

![img_16.png](img_16.png)

Por fim, para fazer a publicação em HTML dos testes é dada esta instrução ao __post__ o qual cria o __index.html__.

--------------------------------
## **CA5 - Alternative - Bitbucket pipeline**

# CircleCi

O __CircleCI__ é uma solução compatível com repositórios __git__, como GitHub, GitHub Enterprise e Atlassian Bitbucket 
por exemplo, testa as compilações virtualizadas e implanta a passagem de compilações para ambientes de destino em cada 
novo commit automaticamente. Também trabalha com aprovação de fluxo de trabalho, a poder pausar manualmente através da
aprovação. 

Suporta diversas linguagens, a incluir Java, entre outras que rode em linux ou macOS. Possuí um serviço de nuvem com 
opção gratuita. Em termos de personalização parece ser menos favorável que o __Jenkins__, mas possui testes e versões 
atualizadas com frenquência. É baseado em clound e tem sua contrução em um ficheiro __circleci.yml__. Possuí alguma 
complexidade a mais na construção do script.

# Bitbucket pipeline

No __Bitbucket pipeline__ não é preciso sincronizar repositórios visto que já é integrado com o próprio bitbucket 
isso inclui o gerenciamento de usuário e não há servidores CI para configurar. Apenas é preciso ativar o Pipelines.

É compatível com diversas linguagens, incluindo Java, JavaScript, PHP, Ruby, Python, .NET Code e outros. Utiliza um
ficheiro bitbucket-pipeline.yml para a construção do script.

Os pipelines podem ser alinhados com a estrutura da ramificação, a facilitar o desenvolvimento com fluxos de trabalho 
em ramificações.