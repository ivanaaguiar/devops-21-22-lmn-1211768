# **DOCUMENTATION**

--------------------------------
## **CA2 - Part1 - GRADLE** 

Gradle consiste em uma ferramente para automatizar o processo de build de um projeto.
Essa ferramenta se responsabiliza pela verificação de dependências externas, como 
bibliotecas, de testes unitários ou de integração, de compilação e empacotamento, como 
arquivos ".jar" ou ".war". A ferramenta Gradle utiliza a linguagem Groovy, que tem
a característica de mais fácil compreensão. Além de ser compatível com diversas linguagens
de programação.

--------------------------------
### **CA2 - Part1 - ALTERNATIVE**

Para uma melhor organização do trabalho, cria-se uma pasta dentro de "CA2" denominada
"part1". Nesta pasta é feito o clone do projeto ___"gradle_basic_demo"___, através do a seguir comando, conforme instruido,
e exclui-se o ficheito ".git" para evitar conflitos no repositório git:

|git clone https://bitbucket.org/luisnogueira/gradle_basic_demo/|
|---|

Quando o projeto gradle é construido, é criado um ficheiro "wrapper" que permite executar as
tarefas sem ser necessária a instalação local do Gradle. A partir disto é possível utilizar
do seguinte comando para executar a versão gradle do projeto.

|gradlew|
|---|

A seguir foram utilizados os seguintes comandos conforme a instrução do ___README.md___ do projeto:

|gradlew build|
|---|

Esse comando permite fazer o build do projeto.

|java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001|
|---|

Esse comando cria o chat servidor do app. Para automatizar esse processo, cria-se
a task ___runServer___ com o seguinte código:

|task runServer(type:JavaExec, dependsOn: classes) {group = "DevOps" description = "Launches a chat server that connects to a client on localhost:59001 " classpath = sourceSets.main.runtimeClasspath mainClass = 'basic_demo.ChatServerApp' args '59001'}|
|---|

Para executar essa task, basta usar os eguinte comando na linha de comando:

|gradlew runServer|
|---|

E para finalizar os comando de instrução, utiliza-se o seguinte comando:

|gradlew runClient|
|---|

Este, por sua vez, inicia um chat com o cliente como era suposto uma vez que o projeto simula a 
comunicação entre o servidor e o cliente.

Conforme as instruções, adiciona-se às dependências o junit para implementação de testes com 
o seguinte código no ficheiro ___build.gradle___:

|testImplementation ‘junit: junit: 4.12’|
|---|

A seguir, adiciona-se uma nova task do tipo copy para fazer uma copia do conteúdo de ___"src"___
em um nova pasta de beckup com o seguinte código:

|task backup(type: Copy) {from "src/" into "build/backup/src_copy"}|
|---|

Para finalizar a primeira parte do exercício CA2-part1, cria-se uma task do tipo zip
para criação de uma pasta zipada para a pasta backup com o seguinte código:

|task zip(type: Zip) {from "src/" archiveName "src_copy.zip" destinationDir(file('build/backup'))}|
|---|

Para executar as tarefas implementadas basta usar o seguinte comando na linha de comando:

|gradlew ___taskName___|
|---|

------------------------------------------------------------
## **CA2 - Part2 - GRADLE**

Em um primeiro momento, cria-se um branch chamado "tut-basic-gradle" para trabalhar com um novo projeto gradle.
Para isso, usa-se os commandos:

|git branch tut-basic-gradle|
|---|

|git push origin tut-basic-gradle|
|---|

Para melhor organizar o trabalho, cria-se a pasta "part2" onde é introduzido o 
conteúdo do ficheiro .zip criado através do link https://start.spring.io/, que 
por sua vez é responsável por adicionar as dependências Rest Repositories, 
Thymeleaf, Spring Data JPA, H2 Database e iniciar o projeto gradle spring boot.
Nesse momento, foram seguidas as instruções do exercício a utilizar a versão 
do Spring Boot 2.6.6 e Java 11.

Em um segundo momento, copia-se os ficheiros indicados do projeto "basic" para 
a pasta ___./CA2/part2___ e exclui-se os ficheiros necessários para não gerar
conflitos.

Em seguida, utiliza-se o comando:

|gradlew bootRun|
|---|

Sem que houve-se erros no funcionamento da applicação, Verifica-se que o endereço 
web ___http://localgost:8080___ não possui nenhum conteúdo tal como previsto nas 
instruções do exercício.

Para que haja interação com o frontend, instala-se o plugin sugerido 
___frontend-gradle-plugin___ implementando o código a seguir na secção de plugins
, de acordo com a versão java utilizada no ficheiro ___build.gradle___.

|id "org.siouan.frontend-jdk11" version "6.0.0"|
|---|

A configuração é realizada ao acrescentar os seguintes códigos:

* No ficheiro ___build.gradle___:

|frontend {nodeVersion = "14.17.3" assembleScript = "run build" cleanScript = "run clean" checkScript = "run check"}|
|---|

* No ficheiro ___packge.json___:

|"scripts": {"webpack": "webpack", "build": "npm run webpack", "check": "echo Checking frontend", "clean": "echo Cleaning frontend", "lint": "echo Linting frontend", "test": "echo Testing frontend"}|
|---|

Após a implementação do código, é feito o build na linha de comando através do commando:

|gradlew build|
|---|

O comando build vai executar a instalação do NODE e será acrescentada
a pasta node ao código do projeto. Uma vez realizado o commando com sucesso é preciso
correr a aplicação novamente para verificar seu funcionamento. Isso é feito através do
seguinte comando:

|gradlew bootRun|
|---|

Assim, verifica-se que já está a aparecer o conteúdo no endereço web ___http://localgost:8080___.

Uma vez que tudo é executado com sucesso, utiliza-se o seguinte comando para verificar
se as "Tasks" estão a ser realizadas:

|gradlew tasks|
|---|

Com tudo a trabalhar, cria-se, conforme solicitado, a task copy, do tipo "copy" e dependsOn "jar" 
com o seguinte código:

|task copyJar (type:Copy, dependsOn: jar) { group = "CA2-part2" from ('build/libs/react-and-sprint-data-rest-basic-0.0.1-SNAPSHOT.jar') into ('dist')}|
|---|

A continuar o solicitado, cria-se a task delete com o seguinte código:

|task delete(type: Delete, dependsOn: build) {group = "Devops" description = "delete all the files generated by webpack" delete 'src/main/resources/static/built'}|
|---|

Para realizar as tasks na linha de comando, basta usar como comando:

|gradlew ___taskName___|
|---|

Ao final da tarefa, com tudo a funcionar corretamente, faz-se o merge do branch 
___tut-basic-gradle___ com o ___master___, para isso utiliza-se os seguintes comandos:

|git checkout master|
|---|

|git marge tut-basic-gradle|
|---|

Sem conflitos, faz-se o comando push para enviar tudo ao repositório remoto.

-----------------------------------------------------------
### **CA2 - Part2 - ALTERNATIVE**

Em alternativa ao Gradle, há o Maven que é a base da construção do Gradle. 

Apresenta sistema de dependências, compilação de multiplos módulos, estrutura de projetos e compilação de 
modelo consistentes. 

Maven é orientado a plugin e mantém uma rotina de compilação. Utiliza o __pom.xml__ (Project Object Model) para
descrever o projeto, seu nome e versão, seus artefactos, localização do código fonte, plugins e dependências.

De modo geral, o Maven tem uma leitura mais dífil que o Gradle pela sua estrutura e organização da linguagem no
__pom.xml__. Gradle permite uma maior praticidade e organização de código para definir as configurações no __gradle.build__.